{-# LANGUAGE OverloadedStrings #-}

-- import qualified Clay as C
-- import qualified Data.Text as T
-- import qualified Data.Text.IO as TIO
-- import qualified Data.Text.Lazy as L
-- import           Lucid
import           System.Environment (getArgs, getProgName)

main :: IO ()
main = do
    args <- getArgs
    if length args /= 2
    then do
        progName <- getProgName
        putStrLn $ "usage: " ++ progName ++ " <input dat> <output html>"
    else do
        print 42

