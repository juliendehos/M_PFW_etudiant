{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field 

main :: IO ()
main = do
    conn <- SQL.open "music.db"

    putStrLn "\n*** id et nom des artistes ***"
    -- TODO

    putStrLn "\n*** id et nom de l'artiste 'Radiohead' ***"
    -- TODO

    putStrLn "\n*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***"
    -- TODO

    putStrLn "\n*** noms des titres (en utilisant le type Name) ***"
    -- TODO

    putStrLn "\n*** noms des titres (en utilisant une liste de T.Text) ***"
    -- TODO

    SQL.close conn

