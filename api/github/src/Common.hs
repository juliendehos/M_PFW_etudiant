{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Common where

import User 

import Data.Text
import Servant

type AgentHeader = Header "User-Agent" Text

type UserApi = "users" :> Capture "user" Text :> AgentHeader :> Get '[JSON] (Maybe User)

type GithubApi = UserApi 

type MyhubApi = UserApi

