{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import Common
import User

import Data.Proxy 
import Data.Text
import Network.HTTP.Client (newManager, defaultManagerSettings)
-- import Servant
import Servant.Client

getUser :: Text -> Maybe Text -> ClientM (Maybe User)
getUser = client (Proxy @MyhubApi)

userAgent :: Maybe Text
userAgent = Just "Servant Client for Myhub"

main :: IO ()
main = do
    mgr <- newManager defaultManagerSettings
    let env = mkClientEnv mgr (BaseUrl Http "localhost" 3000 "")

    runClientM (getUser "juliendehos" userAgent) env >>= print

