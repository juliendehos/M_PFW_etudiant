{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import Common
import User

import Data.Proxy 
import Data.Text
import Network.HTTP.Client (newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
-- import Servant
import Servant.Client

getUser :: Text -> Maybe Text -> ClientM (Maybe User)
getUser = client (Proxy @GithubApi)

userAgent :: Maybe Text
userAgent = Just "Servant Client"

main :: IO ()
main = do
    mgr <- newManager tlsManagerSettings
    let env = mkClientEnv mgr (BaseUrl Https "api.github.com" 443 "")

    runClientM (getUser "juliendehos" userAgent) env >>= print

