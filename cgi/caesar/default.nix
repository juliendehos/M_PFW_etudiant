{ pkgs ? import <nixpkgs> {} }:
let 
  _caesar = pkgs.haskellPackages.callCabal2nix "caesar" ./. {};

  _drv = pkgs.stdenv.mkDerivation {
    name = "caesar";
    src = ./.;
    buildInputs = [ _caesar ];
    installPhase = ''
      mkdir $out
      cp data/* $out/
      cp ${_caesar}/bin/*.cgi $out/
    '';
  };
in

if pkgs.lib.inNixShell then _caesar.env else _drv

