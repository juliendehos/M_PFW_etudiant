{ pkgs ? import <nixpkgs> {} }:

let 
  _echows = pkgs.haskellPackages.callCabal2nix "echows" ./. {};

  _drv = pkgs.stdenv.mkDerivation {
    name = "echows";
    src = ./.;
    buildInputs = [ _echows ];
    installPhase = ''
      mkdir $out
      cp -r client $out/
      cp ${_echows}/bin/echows $out/
    '';
  };

in
if pkgs.lib.inNixShell then _echows.env else _drv

