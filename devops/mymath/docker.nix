{ pkgs ? import <nixpkgs> {} }:
let
  _mymath = pkgs.haskellPackages.callCabal2nix "mymath" ./. {};
  _app = pkgs.haskell.lib.justStaticExecutables _mymath;
in
pkgs.dockerTools.buildLayeredImage {
  name = "mymath";
  tag = "latest";
  config.Cmd = [ "${_app}/bin/mymath-server" ];
}

